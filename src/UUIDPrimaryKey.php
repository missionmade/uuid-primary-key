<?php

namespace MissionMade\UUIDPrimaryKey;

use Ramsey\Uuid\Uuid;

trait UUIDPrimaryKey
{
    protected static function bootUUIDPrimaryKey()
    {
        static::creating(function ($model) {
            if (!$model->getKey()) {
                $model->{$model->getKeyName()} = (string) Uuid::uuid4();
            }
        });
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }
}
